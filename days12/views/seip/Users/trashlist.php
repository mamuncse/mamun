<?php
include_once("../../../vendor/autoload.php");
use App\seip\Users\Users;
$obj = new Users();
$arr = $obj->trashed();
if (isset($_SESSION['message'])) {
    echo $_SESSION['message'];
    unset($_SESSION['message']);
}
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>crud oop $ PDO</title>
</head>
<body>
<div class="page_wrap">
    <div class="heading">
        <h1>Student List</h1>
        <h2><a href="create.php">Add Student</a> ||
            <a href="index.php">original list</a>
        </h2>
    </div>
    <div class="table">
        <table border="1">
            <tr>
                <th width="5%">Serial</th>
                <th width="15%">Name</th>
                <th width="10%">Student_ID</th>
                <th width="20%">Email</th>
                <th width="10">Password</th>
                <th width="10%">Phone</th>
                <th width="7%">Gender</th>
                <th width="22%">Action</th>
            </tr>
            <?php
            $i = 1;
            foreach ($arr as $key => $value) {

                ?>
                <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?php echo $value['name']; ?></td>
                    <td><?php echo $value['std_id']; ?></td>
                    <td><?php echo $value['email']; ?></td>
                    <td><?php echo $value['password']; ?></td>
                    <td><?php echo $value['phone']; ?></td>
                    <td><?php echo $value['gender']; ?></td>
                    <td>
                        <a href="show.php?id=<?php echo $value['id']; ?>">Show info</a> ||
                        <a href="edit.php?id=<?php echo $value['id']; ?>">update</a> ||
                        <a href="delete.php?id=<?php echo $value['id']; ?>"
                           onclick="return confirm('Are your Sure Deleted');">Delete</a>
                        <a href="restore.php?id=<?php echo $value['id']; ?>"
                           onclick="return confirm('Are your Sure restore');">Restore</a>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>
</body>
</html>