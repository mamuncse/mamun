<?php
include_once("../../../vendor/autoload.php");
use App\seip\Users\Users;
$obj=new Users();
$obj->setData($_GET);
$value=$obj->show();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../../../style.css">
    <title>Edit mobile model</title>
</head>
<body>
<div class="page_wrap">
    <div class="heading">
		<h1>Student Form</h1>
		<h2><a href="index.php">Show List</a></h2>
	</div>

<form action="update.php" method="post">
    <label for="name">Student Name</label><input type="text" name="name" id="name" value="<?php echo $value['name'];?>"><br/>
    <label for="Std_id"> Student ID</label><input type="number" name="std_id" id="Std_id" value="<?php echo $value['std_id'];?>"><br/>
    <label for="email"> E-mail</label><input type="email" name="email" id="email" value="<?php echo $value['email'];?>"><br/>
	<label for="pass">password</label><input type="password" name="password" id="pass" value="<?php echo $value['password'];?>"><br/>
    <label for="phone">Phone Number</label><input type="number" name="phone" id="phone" value="<?php echo $value['phone'];?>"><br/>
	<label>Gender</label><input type="radio" name="gender" value="male"<?php if($value['gender']=='male')echo"checked='checked'";?>>Male
	<input type="radio" name="gender" value="female" <?php if($value['gender']=='female')echo"checked='checked'";?>>Female<br/>
     <input type="submit" value="update" />
     <input type="reset" value="Clean"><br/>
     <input type="hidden" name="id" value="<?php echo $value['id']; ?>">
</form>
</div>
</body>
</html>
