<?php
namespace App\seip\users;

use PDO;

class Users
{
    public $id = '';
    public $name = '';
    public $std_id = '';
    public $email = '';
    public $password = '';
    public $phone = '';
    public $gender = '';
    public $pdo = ' ';

    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=students_info', 'root', '');
    }

    public function setData($data = '')
    {
        //print_r($_POST);
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists('std_id', $data)) {
            $this->std_id = $data['std_id'];
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('password', $data)) {
            $this->password = $data['password'];
        }
        if (array_key_exists('phone', $data)) {
            $this->phone = $data['phone'];
        }
        if (array_key_exists('gender', $data)) {
            $this->gender = $data['gender'];
        }
        return $this;
    }

    public function show()
    {

        try {
            $query = "SELECT * FROM `std_users` where unique_id= " . "'" . $this->id . "'";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            return $data;
        } catch (PDOExecption $e) {
            echo "ERROR:" . $e->getMessage();
        }
    }

    public function trash()
    {
        $uni_id = "'" . $this->id . "'";
        try {
            $query = "UPDATE `std_users`
			SET 
			deleted_at = :del 
			WHERE `unique_id`=" . $uni_id;
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ":del" => date('Y-m-d h:m:s'),

                ));
            if ($stmt) {

                $_SESSION['message'] = "<span style='color:green;font-size:20px;'>data trashed successfully!!</span>";
                header("Location:trashlist.php");
            }
        } catch (PDOExecption $e) {
            echo "ERROR:" . $e->getMessage();
        }
    }

    public function trashed()
    {
        try {
            $query = "SELECT * FROM `std_users` WHERE deleted_at !='0000-00-00 00:00:00.00000'";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
            return $data;
        } catch (PDOExecption $e) {
            echo "ERROR:" . $e->getMessage();
        }
        //$data=array('name','std_id','email','passowrd','phone','gender');
        //return $data;
    }

    public function restore()
    {
        try {
            $query = "UPDATE `std_users`
			SET 
			deleted_at = :del 
			WHERE `id`=:id ";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ":del" => '0000-00-00 00:00:00.00000',
                    ":id" => $this->id,

                ));
            if ($stmt) {

                $_SESSION['message'] = "<span style='color:#721380;font-size:20px;'>data Restore successfully!!</span>";
                header("Location:index.php");
            }
        } catch (PDOExecption $e) {
            echo "ERROR:" . $e->getMessage();
        }
    }

    public function update()
    {
        try {
            $query = "UPDATE `std_users`
			SET 
			`name` = :name, 
			`std_id` = :std_id, 
			`email` = :email,
			`password` = :password, 
			`phone` = :phone,
			`gender` = :gender
			WHERE `id`=:id ";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ":id" => $this->id,
                    ":name" => $this->name,
                    ":std_id" => $this->std_id,
                    ":email" => $this->email,
                    ":password" => $this->password,
                    ":phone" => $this->phone,
                    ":gender" => $this->gender
                ));
            if ($stmt) {

                $_SESSION['message'] = "<span style='color:green;font-size:20px;'>data updated successfully!!</span>";
                header("Location:index.php");
            }
        } catch (PDOExecption $e) {
            echo "ERROR:" . $e->getMessage();
        }
    }

    public function delete()
    {
        try {
            $query = "DELETE FROM `std_users` WHERE id = " . $this->id;
            $stmt = $this->pdo->query($query);
            $stmt->execute();
            if ($stmt) {

                $_SESSION['message'] = "<span style='color:red;font-size:20px;'>Successfully Deleted!!</span>";
                header("Location:trashlist.php");
            }
        } catch (PDOExecption $e) {
            echo "ERROR:" . $e->getMessage();
        }
    }

    public function index()
    {
        try {
            $query = "SELECT * FROM `std_users` WHERE deleted_at='0000-00-00 00:00:00.00000'";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
            return $data;
        } catch (PDOExecption $e) {
            echo "ERROR:" . $e->getMessage();
        }
        //$data=array('name','std_id','email','passowrd','phone','gender');
        //return $data;
    }

    public function store()
    {
        try {
            $query = "INSERT INTO `std_users` (`id`,`unique_id`,`name`, `std_id`, `email`, `password`, `phone`, `gender`) VALUES (:id, :uid, :name, :std_id, :email, :password, :phone, :gender)";
            $stmt = $this->pdo->prepare($query);
            $stmt2 = $stmt->execute(
                array(
                    ":id" => null,
                    ":uid" => uniqid(),
                    ":name" => $this->name,
                    ":std_id" => $this->std_id,
                    ":email" => $this->email,
                    ":password" => $this->password,
                    ":phone" => $this->phone,
                    ":gender" => $this->gender,
                ));
            if ($stmt2) {

                $_SESSION['message'] = "<span style='color:green;font-size:20px;'>data inserted successfully!!</span>";
                header("Location:create.php");
            }
        } catch (PDOExecption $e) {
            echo "ERROR:" . $e->getMessage();
        }
    }
}