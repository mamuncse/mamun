<?php
require'config.php';
require'function.php';
require'helpers/format.php';
?>
<?php
$db=new database();
$fm=new formatt();
?>
<!DOCTYPE html>
<html>
<head>
	<title>photo gallery</title>
	<link rel="stylesheet" type="text/css" href="style1.css">
</head>
<body>
<div class="page-wrap">
<header>
	<div class="head">
	<div  class="logo">
		<a href="index.php"><img src="images/logo.png" width="150" height="150"></a>
	</div>
	<div class="name">
		<h1>PHOTO GALLERY</h1>
	</div>
	<div class="search">
		<form action="" method="POST">
			<input type="text" name="search" placeholder="search......." id="searchh">
			<input type="submit" value=">>" name="ok" id="ok">
		</form>
	</div>
	<div class="login">
	<form action="" method="POST">
		<label for="username">Username</label><input type="text" name="username" id="username" required><br/>
		<label for="password">Password</label><input type="password" name="password" id="password" required><br/>
		<input type="submit" value="sign In" name="submit" id="sign" required>
		<a href="create.php" id="new-account" target="_blank">New Account</a>	
		</form>
	</div>
	</div>
</header>
<section>
<nav>
	<div class="top-menu">
	<ul>
		<li><a href="#" class="home">Home</a>
		<ul>
			<li><a href="#">Wallpaper</a></li>
			<li><a href="#">Picture Album</a></li>
			<li><a href="#">Astronomy printshop</a></li>
		</ul>
		</li>
		<li><a href="#">Upload</a>
		<ul>
			<li><a href="">wallpaper</a></li>
			<li><a href="#">Animals</a></li>
			<li><a href="#">Natural</a></li>
			<li><a href="#">Tours</a></li>
			<li><a href="#">Amazing</a></li>
		</ul>
		</li>
		<li><a href="#">About</a>
		<ul>
			<li><a href="#">about ours</a></li>
			<li><a href="#">about website</a></li>
		</ul>
		</li>
		<li><a href="suggestion.php">help</a>
		<ul>
			<li><a href="#">E-mail</a></li>
			<li><a href="#">contract</a></li>
		</ul>
		</li>
	</ul>
	</div>
	</nav>
	<nav>
	<div class="dropdown">
	<h2>menu</h2>
	<ul>
		<li><a href="#">New images</a></li>
		<li><a href="#">Wallpaper</a></li>
		<li><a href="#">amazing images</a></li>
		<li><a href="#">varies photo</a></li>
	</ul>
	</div>
	</nav>
	<?php
	$total_pages=2;
	if (isset($_GET['page'])) {
		$page=$_GET['page'];
	}else{
		$page=1;
	}
	$start_page=($page-1) * $total_pages;
	?>
	<?php
	$query="SELECT * FROM tbl_post limit $start_page,$total_pages";
	$post=$db->select($query);
	if ($post) {
	while ($result=$post->fetch_assoc()) {
			
	?>
		<div class="paragraph">
		<h2><a href="post.php?id=<?php echo $result['id']; ?>"><?php echo $result['title']; ?></a></h2>
		<h4><?php echo $fm->formattDate($result['date']); ?><a href="#"> By </a><?php echo $result['author']; ?></h4>
		<a href="#"><img src="admin/upload/<?php echo $result['image']; ?>" alt="post image"></a>
		<?php echo $fm->textShort($result['body']); ?>
		<a href="post.php?id=<?php echo $result['id'];?>" id="more">Read more</a>

		</div>
		<?php } ?>

		<?php
		$query="select * from tbl_post";
		$result=$db->select($query);
		$total_rows=mysqli_num_rows($result);
		$total_pages_num=ceil($total_rows/$total_pages);
		echo "<span class='pagination'><a href='index.php?page=1'>".'First Page'."</a>";
		for ($i=1; $i<=$total_pages_num; $i++) { 
		echo"<a href='index.php?page=".$i."'>".$i."</a>";
		}
		echo"<a href='index.php?page=$total_pages_num'>".'Last Page'."</a></span>"
		?>
	<?php }else { ?>
		<p>data is not available</p>
		<?php } ?>

	<!---
	<div class="images-slide">
	<ul>
		<li><img src="images/images (1).jpg" width="200px" height="200px" alt="image"></li>
		<li><img src="images/images.jpg" width="200px" height="200px" alt="image"></li>
		<li><img src="images/img/1476531038-E3.jpg" width="200px" height="200px" alt="image"></li>
		<li><img src="images/img/1476531068-E4.jpg" width="200px" height="200px" alt="image"></li>
		<li><img src="images/img/1476531084-E5.jpg" width="200px" height="200px" alt="image"></li>
		<li><img src="images/img/1476531112-E6.jpg" width="200px" height="200px" alt="image"></li>
	</ul>	
	</div>-------->
</section>
<div class="footer">
<footer>
	<h2>Copywrite@2016</h2>
</footer>
</div>
</div>
</body>
</html>