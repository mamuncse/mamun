<?php
class database{
	public $host=DB_HOST;
	public $user=DB_USER;
	public $pass=DB_PASS;
	public $dbname=DB_NAME;
	
	public $link;
	public $error;
	 public function __construct(){
	$this->connectDB();
	 }
	 private function connectDB(){
		 $this->link=new mysqli($this->host,$this->user,$this->pass,$this->dbname);
		 if(!$this->link){
			 $this->error="connection failed".$this->link->connect_error;
		 }
	 }
	 public function select($query){
		 $creat_data=$this->link->query($query) or die($this->link->error.__LINK__);
	 if($creat_data->num_rows>0){
		 return $creat_data;
	 }
	 else{
		 return false;
	 }
	 }
	 public function insert($query){
		 $insert_data=$this->link->query($query) or ($this->link->error.__LINK__);
		 if($insert_data){
			 header("Location:index.php?msg=".urlencode('data inserted successfully'));
			 exit();
		 }
		 else{
			 die("ERROR:(".$this->link->errno.")".$this->link->error);
		 }
	 }
	 public function update($query){
	$update_data=$this->link->query($query) or die ($this->link->error.__LINK__);
	if ($update_data) {
	 header("Location:index.php?msg=".urlencode('data updated successfully'));
			 exit();
	}
	else{
		return false;
	}
}
public function delete($query){
	$delete_data=$this->link->query($query) or die ($this->link->error.__LINK__);
	if ($delete_data) {
	header("Location:index.php?msg=".urlencode('Data deleted successfully!!!'));
		exit();
	}
	else{
		die("ERROR:(".$this->link->errno.")".$this->link->error);
	}
}
 public function login($query){
		 $login_user=$this->link->query($query) or die($this->link->error.__LINK__);
	 if($login_user->num_rows>0){
		header("Location:read.php?msg=".urlencode('Data deleted successfully!!!'));
		exit();
	 }
	 else{
	die("ERROR:(".$this->link->errno.")".$this->link->error);
	 }
}
 public function suggestion($query){
		 $suggestion_data=$this->link->query($query) or die($this->link->error.__LINK__);
		 if($suggestion_data){
			 header("Location:suggestion.php?msg=".urlencode('message send successfully'));
			exit();
		 }
		 else{
			 die("ERROR:(".$this->link->errno.")".$this->link->error);
		 }
	 }
}
?>