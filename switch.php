<?php
$a = 1;
switch ($a) {
    case 1:
        echo "the number is one";
        break;
    case 2:
        echo "the number is two";
        break;
    case 3:
        echo "the number is three";
        break;
    case 4:
        echo "the number is four";
        break;
    case 5:
        echo "the number is five";
    default:
        echo "another number";
}